import 'package:sparkplug/sparkplug.dart';
import 'package:logging/logging.dart';
import 'package:pure/pure.dart';

void main() async {
  Logger.root
    ..level = Level.INFO
    ..onRecord.listen(logHandler);

  await loadSystemDependencies();
  final config = await loadConfig();

  final clients = await buildClients(config);
  final server = buildHttpServer(clients, config);

  await Future.forEach(clients, runBot.flip().apply(config));
  await server.listen(config.port);
}

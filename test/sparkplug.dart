// ignore_for_file: omit_local_variable_types
import 'package:sparkplug/sparkplug.dart';
import 'package:test/test.dart';

void main() {
  group('Parse Json', () {
    test('Default', () {
      Json input = {'msg': 'pass'};
      expect(findKeyValue(default_tag, input), 'pass');
    });
    test('Other key', () {
      Json input = {'data': 'pass'};
      expect(findKeyValue('data', input), 'pass');
    });
    test('Nested', () {
      Json input = {
        'data': 'hello',
        'more': {'msg': 'pass'}
      };
      expect(findKeyValue(default_tag, input), 'pass');
    });
    test('Double Nested', () {
      Json input = {
        'data': 'hello',
        'more': {
          'moreData': {'msg': 'pass'}
        }
      };
      expect(findKeyValue(default_tag, input), 'pass');
    });
    test('No key', () {
      Json input = {
        'data': 'hello',
        'more': {'test': 'pass'}
      };
      expect(findKeyValue(default_tag, input), messageNotFound);
    });
    test('No key Nested', () {
      Json input = {
        'data': 'hello',
        'more': {'test': 'pass'}
      };
      expect(findKeyValue(default_tag, input), messageNotFound);
    });
  });

  group('Tail Recrusion', () {
    test('Small List', () {
      Iterable<String> input = ['one', 'two'];
      int count = 0;

      input.tailRec((item) => count++);
      expect(count, 2);
    });

    test('Large List', () {
      int length = 100000;
      int count = 0;
      Iterable<String> input =
          List.generate(length, (index) => index.toString());

      input.tailRec((item) => count++);
      expect(count, length);
    });
  });
}

# 0.1.1
- Docker image reduced from ~1GB to ~25MB
- Logging bug corrected to show stacktrace
- App exits if base dependencies arent met

# 0.1.0
- Initial beta release

## Upgrading from 0.0.x

# 0.0.7
- Bot registration handled

# 0.0.6
- Json keep can be customized

# 0.0.5
- Ability to have a list of bots
- Ability to have a list of rooms for bots

# 0.0.4
- Auto joining rooms

# 0.0.3
- Yaml config file
- Json encoding
- build_runner added

# 0.0.2
- Dockerized

# 0.0.1
- Posting to homeserver with encryption.

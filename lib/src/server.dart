import 'package:alfred/alfred.dart';
import 'package:logging/logging.dart';
import 'package:matrix/matrix.dart';
import 'package:pure/pure.dart';

import 'const.dart';
import 'extensions.dart';
import 'model/config.dart';
import 'model/domain.dart';

const messageNotFound = 'Message not found.';

Alfred buildHttpServer(List<Client> clients, Config config) {
  final log = Logger('Http')..info('Setting up http server.');
  final app = Alfred()..get('/', (req, res) => {'alive': true});

  clients.tailRec((client) {
    final bot = config.getBot(client.clientName);
    bot.rooms.tailRec((room) => app.post(room.url, (req, res) async {
          log.info('Message received to roomId ${room.id}');
          final msg = (await req.bodyAsJsonMap)
              .pipe(findKeyValue.apply(room.data ?? default_tag));
          if (msg.isNotEmpty && msg != messageNotFound) {
            await client.send(config.domain, room.id, msg);
          } else {
            log.shout(
                '$messageNotFound Double check JSON with key ${room.data}');
          }
        }));
  });

  return app;
}

String findKeyValue(String key, Map<dynamic, dynamic> json) {
  Tram<String> go(Map<dynamic, dynamic> json) {
    if (json.isEmpty) {
      return Tram.done(messageNotFound);
    } else if (json.keys.contains(key)) {
      return Tram.done(json[key]);
    } else {
      final nested = json.values
          .whereType<Json>()
          .fold(<String, dynamic>{}, (_, x) => {...x});
      return nested.isEmpty
          ? Tram.done(messageNotFound)
          : Tram.call(() => go(nested));
    }
  }

  return go.bounce(json);
}

import 'package:logging/logging.dart';
import 'package:matrix/matrix.dart';
import 'package:pure/src/partial_application/extensions.dart';

import 'model/config.dart';
import 'extensions.dart';
import 'util.dart';

Future<List<Client>> buildClients(Config config) async {
  final bots = <Client>[];
  final admins = config.admins.map(matrixUser.flip().apply(config.domain));

  config.bots.tailRec((bot) {
    final client = Client(bot.username, databaseBuilder: _makeDB);
    final log = Logger(client.clientName);

    client.onUiaRequest.stream.listen((uiaRequest) async {
      try {
        log.info('UiaRequest received');
        log.fine('UiaRequest type ${uiaRequest.nextStages.first}');
        await uiaRequest.completeStage(AuthenticationData(
            type: AuthenticationTypes.dummy, session: uiaRequest.session));
      } catch (e, s) {
        log.severe('UiaRequest failed.', e, s);
        uiaRequest.cancel();
      }
    });

    client.onLoginStateChanged.stream.listen((LoginState state) async {
      switch (state) {
        case LoginState.loggedIn:
          final joined = await client.getJoinedRooms();
          log.fine('Joined rooms are $joined');
          if (joined.isNotEmpty) {
            final notJoined = bot.rooms
                .map((room) => room.roomFromId(config.domain))
                .where((id) => !joined.contains(id))
                .toList();
            log.fine('Not joined rooms are $notJoined');
            notJoined.tailRec((id) async => await client.joinRoomById(id));
          }
          break;
        case LoginState.loggedOut:
          log.info('Logged out');
          break;
      }
    });

    client.onSync.stream.listen((SyncUpdate update) async {
      if (update.hasRoomUpdate) {
        log.fine('Sync has room updates');
        final invites = update.rooms?.invite;
        log.fine('Sync has room invies = $invites');
        invites?.keys.tailRec((key) {
          invites[key]?.inviteState?.tailRec((StrippedStateEvent status) async {
            log.fine('Sync sender is ${status.senderId}');
            if (admins.contains(status.senderId)) {
              log.info('Sync sender is an admin, attempting room join.');
              await client.joinRoomById(key);
            }
          });
        });
      }
    });
    bots.add(client);
  });

  return bots;
}

Future<FamedlySdkHiveDatabase> _makeDB(Client client) async {
  final db = FamedlySdkHiveDatabase(client.clientName);
  await db.open();
  return db;
}

Future<void> runBot(Client client, Config config) async {
  final bot = config.getBot(client.clientName);
  final log = Logger(bot.username);
  log.info('Initializing bot.');

  try {
    await client.checkHomeserver(config.getDomain);
    log.info('Home check passed');
  } catch (e, s) {
    return log.severe('Homeserver check failed.', e, s);
  }

  // Matrix server closes connections when requests are made too fast.
  await Future.delayed(Duration(seconds: 2));

  try {
    if (await client.checkUsernameAvailability(bot.username) ?? false) {
      log.info('Bot username is not registered');
      try {
        log.info('Registering');
        await client.uiaRequestBackground((auth) => client.register(
            username: bot.username, password: bot.password, auth: auth));
      } catch (e, s) {
        return log.severe('Registration failed.', e, s);
      }
    }
  } catch (e) {
    log.fine('Bot is already registered.');
  }

  if (!client.isLogged()) {
    try {
      log.info('Bot logging in');
      await client.login(
        LoginType.mLoginPassword,
        identifier: AuthenticationUserIdentifier(
            user: matrixUser(bot.username, config.domain)),
        password: bot.password,
      );
    } catch (e, s) {
      return log.severe('Login failed.', e, s);
    }
  } else {
    log.fine('Bot is already logged in');
  }

  try {
    await client.sync();
  } catch (e, s) {
    return log.severe('Server sync failed.', e, s);
  }
}

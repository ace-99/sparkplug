// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Config _$ConfigFromJson(Map<String, dynamic> json) => Config(
      domain: json['domain'] as String,
      https: json['https'] as bool,
      port: json['port'] as int,
      admins:
          (json['admins'] as List<dynamic>).map((e) => e as String).toList(),
      bots: (json['bots'] as List<dynamic>)
          .map((e) => Bot.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ConfigToJson(Config instance) => <String, dynamic>{
      'domain': instance.domain,
      'https': instance.https,
      'port': instance.port,
      'admins': instance.admins,
      'bots': instance.bots,
    };

Bot _$BotFromJson(Map<String, dynamic> json) => Bot(
      username: json['username'] as String,
      password: json['password'] as String,
      rooms: (json['rooms'] as List<dynamic>)
          .map((e) => Room.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$BotToJson(Bot instance) => <String, dynamic>{
      'username': instance.username,
      'password': instance.password,
      'rooms': instance.rooms,
    };

Room _$RoomFromJson(Map<String, dynamic> json) => Room(
      url: json['url'] as String,
      id: json['id'] as String,
      data: json['data'] as String?,
    );

Map<String, dynamic> _$RoomToJson(Room instance) => <String, dynamic>{
      'url': instance.url,
      'id': instance.id,
      'data': instance.data,
    };

import 'package:json_annotation/json_annotation.dart';
import 'package:pure/pure.dart';

import 'domain.dart';
import '../util.dart';

part 'config.g.dart';

@JsonSerializable()
class Config {
  final String domain;
  final bool https;
  final int port;
  final List<String> admins;
  final List<Bot> bots;

  Config(
      {required this.domain,
      required this.https,
      required this.port,
      required this.admins,
      required this.bots});

  factory Config.fromJson(Json json) => _$ConfigFromJson(json);

  Json toJson() => _$ConfigToJson(this);

  Uri get getDomain => https ? Uri.https(domain, '') : Uri.http(domain, '');

  Bot getBot(String name) => bots.firstWhere((b) => b.username == name);
}

@JsonSerializable()
class Bot {
  final String username, password;
  final List<Room> rooms;

  Bot({required this.username, required this.password, required this.rooms});

  factory Bot.fromJson(Json json) => _$BotFromJson(json);

  Json toJson() => _$BotToJson(this);

  String Function(String) get userFromId => matrixUser.apply(username);
}

@JsonSerializable()
class Room {
  final String url, id;
  final String? data;

  Room({required this.url, required this.id, this.data});

  factory Room.fromJson(Json json) => _$RoomFromJson(json);

  Json toJson() => _$RoomToJson(this);

  String Function(String) get roomFromId => matrixRoom.apply(id);
}

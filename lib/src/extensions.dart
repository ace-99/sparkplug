import 'package:matrix/matrix.dart';
import 'package:pure/pure.dart';

import 'util.dart';

extension ClientHelpers on Client {
  Future<String?> send(String domain, String roomId, String msg) async =>
      await getRoomById(matrixRoom(roomId, domain))?.sendTextEvent(msg);
}

extension FP<A, B, C> on C Function(A, B) {
  C Function(B, A) flip() => (B b, A a) => this.call(a, b);
}

extension TailRec<A> on Iterable<A> {
  void tailRec(void Function(A a) f) {
    Tram<void> go(Iterable<A> items) {
      if (items.isEmpty) {
        return Tram.done({});
      } else {
        f(items.toList()[0]);
        return Tram.call(() => go(items.skip(1)));
      }
    }

    return go.bounce(this);
  }

  void tailRecAsync(Future<void> Function(A a) f) =>
      tailRec((a) async => await f(a));
}

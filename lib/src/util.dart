import 'dart:convert';
import 'dart:io';

import 'package:hive/hive.dart';
import 'package:logging/logging.dart';
import 'package:pure/src/composition/dot.dart';
import 'package:yaml/yaml.dart' as yaml;
import 'package:olm/olm.dart' as olm;

import 'const.dart';
import 'model/config.dart';

final matrixUser = (String user, String domain) => '@$user:$domain';
final matrixRoom = (String id, String domain) => '$id:$domain';

Future<Config> loadConfig() async {
  final log = Logger('Config')..info('Loading config from $config_path');
  try {
    final config = await File(config_path)
        .readAsString()
        .then(json.decode.dot(json.encode).dot(yaml.loadYamlNode));
    return Config.fromJson(config);
  } catch (e, s) {
    log.severe('Loading config failed.', e, s);
    exit(1);
  }
}

Future<void> loadSystemDependencies() async {
  final log = Logger('Dependencies');
  try {
    log.info('Setting up database path to $db_path');
    Hive.init(db_path);
    log.info('Checking system for olm library');
    await olm.init();
  } catch (e, s) {
    log.severe('System dependencies failed.', e, s);
    exit(1);
  }
}

void logHandler(LogRecord log) {
  final message = '[${log.loggerName}] - ${log.message}';
  if (log.stackTrace != null) {
    message + ' - ${log.stackTrace}';
  }
  print('${log.time} - |${log.level}| - $message');
}

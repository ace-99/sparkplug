# Compile binary
FROM dart:2.16-sdk AS build

# Resolve app dependencies.
WORKDIR /app
COPY pubspec.* ./
RUN dart pub get

# Copy app source code and AOT compile it.
COPY . .
RUN mkdir build

# Ensure packages are still up-to-date if anything has changed
RUN dart pub get --offline
RUN dart run build_runner build --delete-conflicting-outputs
RUN dart compile exe bin/sparkplug.dart -o build/sparkplug

# Build smaller image to serve the binary
FROM alpine

# Resolve system dependencies.
WORKDIR /app
RUN apk update && apk add olm
RUN mkdir db

# Move AOT-compiled `sparkplug` and required system libraries and configuration
# files stored in `/runtime/` from the build stage.
COPY --from=build /runtime/ /
COPY --from=build /app/build/sparkplug /app/bin/

# Start server.
EXPOSE 8000
CMD ["/app/bin/sparkplug"]

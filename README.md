# Sparkplug

A service that handles Matrix bot deployment, and posts end to end encrypted
messages via webhooks to configured rooms.

Control as many bots as you want, that are able to notify as many rooms as you
want, all from one place. Posting json to endpoints you specify will
trigger the correct bot to notify a Matrix room with the specified message. 

# Table of Contents
- [Usages](#usages)
- [Features](#features)
- [Getting Started](#getting-started)
  - [Dependencies](#dependencies)
  - [Installing](#installing)
  - [Configuration](#configuration)
- [Known Issues](#known-issues)
- [Development](#development)
  - [Dev Dependencies](#dev-dependencies)
  - [Running Tests](#running-tests)
- [License](#license)
- [Motivation](#motivation)
- [Acknowledgments](#acknowledgments)

# Usages

- Its json... keep track / up to date on whatever you want to throw at it
- Stay updated on the health of remote systems
- Use from within your homelab for custom self hosted mobile alerts
- Cronjob scripts to do tasks and report their results
- Integrate with other bots for alerts on GPU stock

# Features

- Bots will register if the username is not taken, if it is then 
  a regular login is completed.
- Bots will accept room invites under two circumstances:
  - When a user that is under the `admin` list has invited them
  - When they are configured to join a `room` and it is invite only, anyone can
    send the invite.
- Multiple rooms used for different types of notifications allow the user to
  fine tune notification preferences on the client of their choosing.
- Json from a webhook you did not create and can not edit will be parsed for
  the correct json `key` supplied in the config to use as the message.
- Multiple ways to deploy to fit your exact needs.
  - Use one config file with multiple bots each having as many rooms as you
    want
  - Use multiple docker containers with multiple config files to micro manage
    bots
  - Have one bot be responsible to sending to many rooms or have each room have
    its own bot

## Getting Started

These instructions will get you a copy of the project up and running on your
server. 

### Dependencies

Requirements for the software and other tools to build.
- [Docker](https://www.docker.com/)
- [Docker-Compose](https://github.com/docker/compose)
- [Matrix Instance](https://github.com/matrix-org/synapse)

### Installing

``` shell
# Clone the repository
git clone https://gitlab.com/ace-99/sparkplug.git
cd sparkplug

# Configure the `config.yml`
cp config.yml.example config.yml

# Configure `docker-compose.yml`
cp docker-compose.yml.example docker-compose.yml

# Run docker in the root of the project
docker-compose up -d

# Test that it worked
## Server check
curl http://<yourserver>:8000

## Message check
curl --header "Content-Type: application/json" \
  --request POST \ 
  --data '{"msg":"It worked!"}' \
  http://<yourserver>:8000/<configured endpoint>

# Check the logs
docker logs --follow <container id>

```
> A dockerhub image may come in the future.

## Configuration

Please read the comments in the example config.yml.example carefully.  Some
additional notes to the ones mentioned in that file:

The 'data' value in each room is the json key to look for in the post request
the server received.  This **defaults** to 'msg' if not specified.  Default or
not the server will recursively search for the specified tag and use the
_first_ match it finds as the message to send to the Matrix server.  If no tag
is found, the bot will post a message saying that.  Refer to the test file for
exact examples.  

## Known Issues

- Every time you bring the container up, a new `session` is created for the
  bot.  This is a matrix session and a new key pair is created.  Bots handle
  sending the new keys.  Building up a lot of sessions is not a _bad_ thing but
  it can look sloppy, a solution may come eventually.
- Depending on your instance of Matrix, bots may not complete registration
  properly.  Registration involves a few Http verification steps to be done and
  if a connection is closed during this process, it will fail.  Restart the
  container if this is the case, and if it still does not work simply make
  the bot account yourself.

## Development

Running as a stand alone dart binary is the quickest way to test.

### Dev Dependencies

- [Dart](https://dart.dev/get-dart)

Configure the `config.yml` files.

Run binary without docker.

`dart run`

### Running Tests

The final test will take some time to complete, this is intentional.

`dart run test`

## License

This project is licensed under the [MIT License](LICENSE.md)

## Motivation

This was a solution to the want and need to have a self hosted and secure
notification system, available on any phone computer or browser.

Side-effects of that:
  - Learn the Dart language
  - Get better using Docker / Docker-Compose
  - Understand more about Matrix protocols and E2EE

## Acknowledgments

- [Famedly](https://gitlab.com/famedly/company/frontend/famedlysdk) - Dart
  Matrix SDK
- [immanuelfodor](https://github.com/immanuelfodor/matrix-encrypted-webhooks) -
  Project sparked the idea
